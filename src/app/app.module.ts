import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { NewCocktailComponent } from './cocktails/new-cocktail/new-cocktail.component';
import { ModalComponent } from './ui/modal/modal.component';
import { ValidateUrlDirective } from './validate-url.directive';
import { CocktailService } from './shared/cocktail.service';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    CocktailsComponent,
    NewCocktailComponent,
    ModalComponent,
    ValidateUrlDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
