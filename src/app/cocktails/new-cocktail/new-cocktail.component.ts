import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { CocktailService } from '../../shared/cocktail.service';
import { urlValidator } from '../../validate-url.directive';
import { Cocktail } from '../../shared/cocktail.model';

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.css']
})
export class NewCocktailComponent implements OnInit {
  cocktailForm!: FormGroup;
  isUploading = false;
  cocktailUploadingSubscription!: Subscription;

  constructor(
    private cocktailService: CocktailService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imageUrl: new FormControl('', [
        Validators.required,
        urlValidator
      ]),
      type: new FormControl('', Validators.required),
      description: new FormControl(''),
      ingredients: new FormArray([]),
      instructions: new FormControl('', Validators.required),
    });

    this.cocktailUploadingSubscription = this.cocktailService.cocktailUploading
      .subscribe((isUploading: boolean) => {
        this.isUploading = isUploading;
      });

  }

  onSubmit() {
    const id = Math.random().toString();
    const cocktail = new Cocktail(
      id,
      this.cocktailForm.get('name')?.value,
      this.cocktailForm.get('imageUrl')?.value,
      this.cocktailForm.get('type')?.value,
      this.cocktailForm.get('description')?.value,
      this.cocktailForm.get('ingredients')?.value,
      this.cocktailForm.get('instructions')?.value,
    );

    const next = () => {
      this.cocktailService.fetchCocktails();
      void this.router.navigate(['/']);
    };

    this.cocktailService.addCocktail(cocktail).subscribe(next);
  }

  addIngredient() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingredientGroup = new FormGroup({
      name: new  FormControl('', Validators.required),
      quantity: new FormControl(null, Validators.required),
      unitOfMeasure: new FormControl('', Validators.required)
    });
    ingredients.push(ingredientGroup);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  ingredientHasError(fieldName: AbstractControl, errorType: string, imageOrDescription: string) {
    const ing = <FormGroup>fieldName;
    return Boolean(ing && ing.touched && ing.controls[imageOrDescription].errors?.[errorType]);
  }

  resetIngredients() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    ingredients.clear();
  }

  deleteIngredient(index: number) {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    ingredients.removeAt(index);
  }

  getIngredientControls() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  resetType() {
    this.cocktailForm.get('type')?.patchValue('');
  }
}
