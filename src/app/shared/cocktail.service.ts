import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Cocktail } from './cocktail.model';

@Injectable()

export class CocktailService {
  cocktailsChange = new Subject<Cocktail[]>();
  cocktailsFetching = new Subject<boolean>();
  cocktailUploading = new Subject<boolean>();

  private cocktails: Cocktail[] = [];

  constructor(private http: HttpClient) {}

  getCocktails() {
    return this.cocktails.slice();
  }

  fetchCocktails() {
    this.cocktailsFetching.next(true);
    this.http.get<{ [id: string]: Cocktail }>('https://plovo-57f89-default-rtdb.firebaseio.com/cocktails.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const cocktailData = result[id];
          return new Cocktail(id, cocktailData.name, cocktailData.imageUrl, cocktailData.type, cocktailData.description,
            cocktailData.ingredients, cocktailData.instructions);
        });
      }))
      .subscribe(cocktails => {
        this.cocktails = cocktails;
        this.cocktailsChange.next(this.cocktails.slice());
        this.cocktailsFetching.next(false);
      }, () => {
        this.cocktailsFetching.next(false);
      });
  }

  addCocktail(cocktail: Cocktail) {
    const body = {
      name: cocktail.name,
      imageUrl: cocktail.imageUrl,
      description: cocktail.description,
      type: cocktail.type,
      ingredients: cocktail.ingredients,
      instructions: cocktail.instructions,
    };

    this.cocktailUploading.next(true);

    return this.http.post('https://plovo-57f89-default-rtdb.firebaseio.com/cocktails.json', body).pipe(
      tap(() => {
        this.cocktailUploading.next(false);
      }, () => {
        this.cocktailUploading.next(false);
      })
    );
  }


}
